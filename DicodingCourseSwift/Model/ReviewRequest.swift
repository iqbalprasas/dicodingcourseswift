//
//  ReviewRequest.swift
//  DicodingCourseSwift
//
//  Created by ARS on 06/07/20.
//  Copyright © 2020 ARS. All rights reserved.
//

import Foundation

struct ReviewRequest: Codable {
    let value: Double
}
