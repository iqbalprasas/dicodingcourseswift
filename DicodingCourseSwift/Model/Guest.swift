//
//  Guest.swift
//  DicodingCourseSwift
//
//  Created by ARS on 06/07/20.
//  Copyright © 2020 ARS. All rights reserved.
//

import Foundation

struct Guest: Codable {
    let success: Bool
    let guestSessionId: String
    
    enum CodingKeys: String, CodingKey {
        case success
        case guestSessionId = "guest_session_id"
    }
}
