//
//  Movies.swift
//  DicodingCourseSwift
//
//  Created by ARS on 06/07/20.
//  Copyright © 2020 ARS. All rights reserved.
//

import UIKit

struct Movies: Codable{
    let page: Int
    let totalResults: Int
    let totalPages: Int
    let movies: [Movie]
    
    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case movies = "results"
    }
}
