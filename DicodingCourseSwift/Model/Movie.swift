//
//  Movie.swift
//  DicodingCourseSwift
//
//  Created by ARS on 06/07/20.
//  Copyright © 2020 ARS. All rights reserved.
//

import UIKit

struct Movie: Codable {
    let popularity: Double
    let posterPath: String
    let title: String
    let genres: [Int]
    let voteAverage: Double
    let overview: String
    let releaseDate: Date
    
    enum CodingKeys: String, CodingKey {
        case popularity
        case posterPath = "poster_path"
        case title
        case genres = "genre_ids"
        case voteAverage = "vote_average"
        case overview
        case releaseDate = "release_date"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
              
        let path = try container.decode(String.self, forKey: .posterPath)
              
        let dateString = try container.decode(String.self, forKey: .releaseDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateString)!
              
        popularity = try container.decode(Double.self, forKey: .popularity)
        
        posterPath = "https://image.tmdb.org/t/p/w300\(path)"
              
        title = try container.decode(String.self, forKey: .title)
        genres = try container.decode([Int].self, forKey: .genres)
        voteAverage = try container.decode(Double.self, forKey: .voteAverage)
        overview = try container.decode(String.self, forKey: .overview)
              
        releaseDate = date
    }
}
