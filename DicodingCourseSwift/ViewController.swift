//
//  ViewController.swift
//  DicodingCourseSwift
//
//  Created by ARS on 06/07/20.
//  Copyright © 2020 ARS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let apiKey = "4a62c9fe8e3d8a4656e41fcb604d9bed";
    let language = "en-US";
    let page = "1";

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getGuestSessionId { (guest) in
            self.postRatingMovie(guest: guest)
        }
        
        // getPopularMovie()
    }
    
    private func postRatingMovie(guest: Guest) {
        var component = URLComponents(string: "https://api.themoviedb.org/3/movie/339095/rating")!
        component.queryItems = [
            URLQueryItem(name: "api_key", value: self.apiKey),
            URLQueryItem(name: "guest_session_id", value: guest.guestSessionId)
        ]
        
        var request = URLRequest(url: component.url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        //let jsonRequest = [
        //    "value": 8.5
        //]
        //let jsonData = try! JSONSerialization.data(withJSONObject: jsonRequest, options: [])
        
        let jsonRequest = ReviewRequest(value: 8.5)
        let jsonData = try! JSONEncoder().encode(jsonRequest)
        
        let task = URLSession.shared.uploadTask(with: request, from: jsonData) { data, response, error in
            guard let response = response as? HTTPURLResponse, let data = data else { return }
            
            if response.statusCode == 201 {
                print("DATA: \(data)")
            }
        }
        
        task.resume()
    }
        
    private func getGuestSessionId(completion: ((Guest) -> ())?) {
        var component = URLComponents(string: "https://api.themoviedb.org/3/authentication/guest_session/new")!
        component.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey)
        ]
        
        let request = URLRequest(url: component.url!)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let response = response as? HTTPURLResponse else {return}
            
            if response.statusCode == 200 {
                let decoder = JSONDecoder()
                let response = try! decoder.decode(Guest.self, from: data!)
                
                completion?(response)
            } else {
                print("ERROR: \(String(describing: data)), HTTP Status: \(response.statusCode)")
            }
        }
        
        task.resume()
    }

    private func getPopularMovie() {
        var urlComponent = URLComponents(string: "https://api.themoviedb.org/3/movie/popular")!
        urlComponent.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "language", value: language),
            URLQueryItem(name: "page", value: page),
        ];
        
        let request = URLRequest(url: urlComponent.url!)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let response = response as? HTTPURLResponse else {return}
            
            if let data = data {
                if response.statusCode == 200 {
                    self.decodeJSON(data: data)
                } else {
                    print("Error : \(data), HTTP status code : \(response.statusCode)")
                }
            }
        }
        
        task.resume()
    }

    private func decodeJSON(data: Data) {
        let decoder = JSONDecoder()
        
        guard let movies = try? decoder.decode(Movies.self, from: data) else {return}
        
        print("PAGE: \(movies.page)")
        print("TOTAL RESULTS: \(movies.totalResults)")
        print("TOTAL PAGES: \(movies.totalPages)")
        movies.movies.forEach { (movie) in
            print("TITLE: \(movie.title)")
            print("POSTER: \(movie.posterPath)")
            print("DATE: \(movie.releaseDate)")
        }
    }
}

